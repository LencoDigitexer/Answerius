<p align="center">
  <img src="images/logo.png" alt="Logo"></img>

</p>
<p align="center">
  <a href="https://addons.mozilla.org/addon/answerius/"><img src="https://img.shields.io/amo/users/answerius?style=flat-square" alt="Badge"></img></a>
</p>

<h1 align="center">ZeroEyes (Answerius)</h1>
<p>Расширение, которое поможет сделать запрос любой информации через поисковики, которые уважают вашу <b>приватность и анонимность</b>.</p>
<p>Добавляет кнопки поисковиков, где вы выбираете через какой вы хотите сделать текущий запрос. Расширение считывает запрос и открывает его в выбранном поисковике.</p>
<p>Список поисковиков доступных для использования:</p>
<ul>
<li>DuckDuckGo</li>
<li>Swisscows</li>
<li>SearXNG</li>
<li>Lukol</li>
<li>Brave Search</li>
<li>MetaGer</li>
<!-- <li>Peekier (Добавится в будущем)</li> -->
<!-- <li>Ecosia (Добавится в будущем)</li> -->
</ul>
<p>Пока не поддерживает такие поисковики как - <b>Peekier</b> и <b>некоторые другие</b>. В них вы просто не увидите кнопки для запроса в другие поисковики.</p>

<h2>Пример использования:</h2>

![image](images/screen3.png)

**Нажимаем поисковик, который хотим использовать** 

![image](images/screen4.png)

**Итоговый результат**
